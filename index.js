const express = require('express');
const app = express();

const bodyParser = require('body-parser');

const path = require('path');


let db = require('diskdb');

db = db.connect('./mydb', ['sections', 'users']);

const multer = require('multer')


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
})


const upload = multer({ storage })

const bcrypt = require('bcrypt');
const saltRounds = 10;
const JWTSECRET = "VIFJFNVDFF!@##frfgfvertr--0}{{!@#$%توانا بود هر که دانا بود ز دانش دل پیر برنا بودfddss}}s|S"
const JWT = require('jsonwebtoken')

app.use('/assets', express.static(path.join(__dirname, 'assets')))
app.use('/uploads', express.static(path.join(__dirname, 'uploads')))

app.use('/modules', express.static(path.join(__dirname, 'node_modules')))

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))


app.set('view engine', 'ejs');

app.use((req, res, next) => {
    res.locals.error = false
    res.locals.token = null
    next()
})


async function CheckJWT(req, res, next) {
    try {
        const user = await JWT.verify(req.headers.token, JWTSECRET)

        if (!user) {
            return res.json({
                status: false,
                message: "این عملیات به دلایل امنیتی محدود شده است"
            })
        }
        req.user = user

        next()
    } catch (error) {
        return res.json({
            status: false,
            message: "این عملیات به دلایل امنیتی محدود شده است"
        })
    }
}



// route is api route and secured by jwt middleware
const route = express.Router()


route.use(CheckJWT)



app.use('/api', route)

app.post("/register", async (req, res) => {

    try {
        const founded = db.users.findOne({
            email: req.body.email,
        })
        if (founded) {
            return res.json({
                status: false,
                message: "این کاربر از قبل در سیستم وجود دارد"
            })
        }
        const hashed = await bcrypt.hash(req.body.password, saltRounds)
        const data = db.users.save({
            email: req.body.email,
            password: hashed
        })
        res.json({
            status: true,
            message: "کاربر با موفقیت ثبت شد",
            data: {
                email: data.email
            }
        })
    } catch (error) {

        throw error
    }

})



app.get('/login', (req, res) => {
    res.render('login')
})

app.post('/login', async (req, res) => {


    try {

        if ((!req.body.email) && (!req.body.password)) {
            return loginErrorMessage(res);
        }
        const user = db.users.findOne({ email: req.body.email })
        if (!user) {
            return loginErrorMessage(res);
        }
        const result = await bcrypt.compare(req.body.password, user.password)
        if (!result) {
            return loginErrorMessage(res);
        }

        const token = JWT.sign({
            email: user.email,
            date: Date.now(),
        }, JWTSECRET)

        res.render('login', {
            error: false,
            token: token,
            message: "ورود موفقیت آمیز"
        })
    } catch (error) {

        console.log(error)

    }
})


app.get('/', (req, res) => {

    let data = db.sections.find({})

    const result = {

    }

    data.forEach(elem => {

        result[elem.section] = elem
    })

    res.render('web', result)
})


app.get('/panel', (req, res) => {

    let data = db.sections.find({})

    const result = {

    }

    data.forEach(elem => {

        result[elem.section] = elem
    })

    res.render('panel', {
        data: result,
        page: req.query.page || ''
    })
})


route.post('/upload', upload.single('file'), function (req, res, next) {
    res.json({
        status: true,
        file: req.file
    })
})


route.post('/put', async (req, res, next) => {

    console.log(req.body)

    const { section, data } = GetData(req.body)

    const query = {
        section: section
    }

    data.section = section
    const result = db.sections.update(query, data, {
        upsert: true
    });

    JSONResponse({
        status: true,
        data: result

    }, res, next)

})


function loginErrorMessage(res) {
    res.render('login', {
        error: true,
        message: "کاربری با این مشخصات یافت نشد"
    });
}

function JSONResponse(data, res, next) {

    res.json(data);
    next()
}

function GetData(body) {
    const section = body.section
    const data = body.data

    return { section, data }
}


app.listen(3000)





